#!/usr/bin/env bash

AUR_BUILDER_TAG="aur-builder"
AUR_BUILD_DIR="$1"

pacman -Syu --needed --noconfirm mesa vulkan-intel docker

docker build -t "$AUR_BUILDER_TAG" - < Dockerfile

sed -i '/[(][)]\|nvidia/!s/^\(MODULES=[(].*\)\([)]\)/\1 \2/;/nvidia/!s/^\(MODULES=[(].*\)\([)]\)/\1i915 nvidia nvidia_modeset nvidia_uvm nvidia_drm\2/' /etc/mkinitcpio.conf

cp etc/modprobe.d/nvidia-graphics.conf /etc/modprobe.d/nvidia-graphics.conf
cp etc/pacman.d/hooks/nvidia-graphics.hook /etc/pacman.d/hooks/nvidia-graphics.hook

docker run -it --rm --volume "$AUR_BUILD_DIR:$AUR_BUILD_DIR:rw" "$AUR_BUILDER_TAG" "yay --builddir $AUR_BUILD_DIR -Syu --needed --noconfirm nvidia-470xx-utils nvidia-470xx-dkms opencl-nvidia-470xx nvidia-470xx-settings libxnvctrl-470xx"

pacman -U --needed --noconfirm "${AUR_BUILD_DIR}/*/*.zst"
