FROM archlinux:base-devel

RUN pacman -Syu --needed --noconfirm git \
 && useradd -m builder \
 && printf "\nbuilder ALL=(ALL) NOPASSWD: /usr/bin/pacman" >> /etc/sudoers \
 && sudo -u builder -i bash -c "\
        git clone https://aur.archlinux.org/yay.git /tmp/yay \
     && cd /tmp/yay \
     && makepkg --install --noconfirm --rmdeps --clean --syncdeps" \
 && rm -rf /tmp/yay \
 && pacman -Sc --noconfirm

ENTRYPOINT ["/usr/bin/sudo", "--login", "--user=builder", "/usr/bin/bash", "-c"]

CMD ["bash"]